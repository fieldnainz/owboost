

Connect-MgGraph -Scopes "AuditLog.Read.All" #, "User.ReadWrite.All" for user disabling. Commented out to avoid accidental user disabling.

$currentDate = Get-Date # Get todays date and store it in variable
$users = Get-MgUser # Get all users and store it in variable

# Loop through all users
foreach ($user in $users) {
    
    $signInActivities = Get-MgAuditLogSignIn # Get all signin activities

    
    $userSignInActivities = $signInActivities | Where-Object { $_.UserId -eq $user.Id } # Filter sign-in activities by user id

    # Order signin activities by CreatedDateTime in descending order and select the first one
    $lastSignIn = $userSignInActivities | Sort-Object CreatedDateTime -Descending | Select-Object -First 1 

    # If user hasn't signed in for 60 days, disable the user and write a message
    if ($lastSignIn.CreatedDateTime -le $currentDate.AddDays(-60)) {
        Set-MgUser -UserId $user.Id -AccountEnabled $false
        Write-Output "User $($user.DisplayName) hasn't signed in for 60 days and has been disabled."
    }
}

Disconnect-MgGraph # Disconnect from Microsoft Graph for security reasons