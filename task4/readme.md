# Setup Instructions

This README.md will walk you through the process of setting necessary variables for this playbook to run.

## Step 1: Generate Ansible Vault Password

First, we need to generate a password for Ansible Vault. This password will be used to encrypt sensitive data.

You can generate a password using the `openssl` command:

```sh
openssl rand -base64 24
```

This command will output a random string that can be used as the Vault password.

Save this password in the vault_password file

```sh
echo 'your_generated_password' > vault_password
```

## Step 2: Set `ansible_become_password` for Remote Host

Next, we need to set the `ansible_become_password` for the remote host. This is the password that Ansible will use to elevate priviliges.

First, encrypt the remote host's password using Ansible Vault:

```sh
ansible-vault encrypt_string 'remote_host_password' --name 'ansible_become_password'
```

Replace `'remote_host_password'` with the actual password of the remote host.

This command will output an encrypted string. Copy this string and paste it into the `ansible_become_password` field in the ``inventories/production/host_vars/remote.yml``

## Step 3: Set `ansible_become_password` for Localhost

Finally, we need to set the `ansible_become_password` for localhost. This is the password that Ansible will use to become the `root` user on localhost.

Encrypt the localhost's password using Ansible Vault:

```sh
ansible-vault encrypt_string 'localhost_password' --name 'ansible_become_password'
```

Replace `'localhost_password'` with the actual password of localhost.

Copy the encrypted string and paste it into the `ansible_become_password` field in the ``inventories/production/group_vars/all.yml``

## Step 4: Set target users on remote host

``inventories/production/host_vars/remote.yml`` Contains a variable `users_to_extract` which needs to be populated with a list of users you want to extract authorized keys from. 



## Step 5: Running the playbook

```sh
ansible-playbook -i inventories/production site.yml
```

# What does it do?

When executed, Ansible will connect to your remote host and fetch contents from ~/.ssh/authorized_keys file. Afterwards the contents are encrypted using Ansible Vault. To read the contents of the file fetched, execute 

```sh
sudo ansible-vault decrypt sshkeys/HOST_USER_authorized_keys
```

```sh
sudo cat sshkeys/HOST_USER_authorized_keys
```

# Yes, there is a commit with an vault_password file with contents in it.

Oops! Ordinarily these files should be placed in .gitignore file if pushing to a repository. Don't push your passwords to a public repository! 