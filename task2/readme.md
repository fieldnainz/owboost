# Task 2
#### Explain the difference between interactive and non-interactive login in Microsot Azure AD (Entra ID). Try to give a real-world example of each login type from your day-to-day life.


##
Interactive login requires user involvement for authentication, while non-interactive login allows applications or scripts to authenticate without user interaction.
##

#### Real-word example for Interactive login
Would be when you open up a web browser to login into Azure portal. You proceed to click sign-in and enter your e-mail, password and MFA.

#### Real-world example for Non-Interactive login
Pipelines would use a secret or a certificate to authenticate to Azure AD, allowing it manage resources without user interaction.
