#!/bin/bash

# Create the user and group "TheApp", and the user "logreader"

useradd --system --no-create-home TheApp
groupadd TheApp
useradd --system --no-create-home logreader
usermod -aG TheApp TheApp

# Create the directory and subdirectory
mkdir -p /TheApp/logs

# Change the owner and group of the directory and subdirectory to "TheApp"
chown -R TheApp:TheApp /TheApp

# Change the permissions of the directory and subdirectory to 770 (full access for owner and group, no access for others)
chmod -R 770 /TheApp

# Set ACL for "logreader" on "/TheApp/logs" directory
setfacl -m u:logreader:r-x /TheApp/logs
