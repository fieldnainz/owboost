# Create the user and group "TheApp" if they do not exist

useradd --system --no-create-home TheApp
groupadd TheApp
useradd --system --no-create-home logreader
groupadd loggroup


# Add "TheApp" and "logreader" to "loggroup"
usermod -aG loggroup TheApp
usermod -aG loggroup logreader

# Create the directory and subdirectory
mkdir -p /TheApp/logs

# Change the owner of the directory and subdirectory to "TheApp" and group to "loggroup"
chown -R TheApp:loggroup /TheApp

# Change the permissions of the directory and subdirectory to 770 (full access for owner and group, no access for others)
chmod -R 770 /TheApp